AWSTemplateFormatVersion: 2010-09-09
Description: Course Project network components. Version 1.0.0
Parameters:
  ipBlock:
    Type: String
    Default: 172.16.0.0/16
    AllowedValues:
      - 10.0.0.0/16
      - 172.16.0.0/16
      - 172.17.0.0/16
      - 172.18.0.0/16
      - 172.19.0.0/16
      - 172.20.0.0/16
      - 172.21.0.0/16
      - 172.22.0.0/16
      - 172.23.0.0/16
      - 172.24.0.0/16
      - 172.25.0.0/16
      - 172.26.0.0/16
      - 172.27.0.0/16
      - 172.28.0.0/16
      - 172.29.0.0/16
      - 172.30.0.0/16
      - 172.31.0.0/16
      - 192.168.0.0/20
  NumberOfNetworks:
    Type: Number
    Default: 256
    MaxValue: 256
    MinValue: 1
  cidrBits:
    Type: Number
    Default: 8
    MinValue: 1
    MaxValue: 32
Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      EnableDnsSupport: true
      EnableDnsHostnames: true
      CidrBlock: !Ref ipBlock
      Tags:
        - Key: Name
          Value: "Course Project VPC"
        - Key: Project
          Value: "Course Project"

  # < Internet Gateway
  internetGateway:
    Type: AWS::EC2::InternetGateway
    Properties:
      Tags:
        - Key: Name
          Value: "Course Project Internet Gateway"
        - Key: Project
          Value: Course Project

  attachInternetGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      VpcId: !Ref VPC
      InternetGatewayId: !Ref internetGateway
  # Internet Gateway >

  # < Subnets
  publicSubnet0:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      MapPublicIpOnLaunch: true
      CidrBlock: 
        Fn::Select:
          - 0
          - !Cidr [ !Ref ipBlock , !Ref NumberOfNetworks, !Ref cidrBits ]
      AvailabilityZone: 
        Fn::Select:
          - 0
          - Fn::GetAZs: 
              Ref: "AWS::Region"
      Tags:
        - Key: Name
          Value: "Course Project public subnet 0"
        - Key: Project
          Value: Course Project

  publicSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      MapPublicIpOnLaunch: true
      CidrBlock: 
        Fn::Select:
          - 1
          - !Cidr [ !Ref ipBlock , !Ref NumberOfNetworks, !Ref cidrBits ]
      AvailabilityZone: 
        Fn::Select:
          - 1
          - Fn::GetAZs: 
              Ref: "AWS::Region"
      Tags:
        - Key: Name
          Value: "Course Project public subnet 1"
        - Key: Project
          Value: Course Project

  publicSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      MapPublicIpOnLaunch: true
      CidrBlock: 
        Fn::Select:
          - 2
          - !Cidr [ !Ref ipBlock , !Ref NumberOfNetworks, !Ref cidrBits ]
      AvailabilityZone: 
        Fn::Select:
          - 2
          - Fn::GetAZs: 
              Ref: "AWS::Region"
      Tags:
        - Key: Name
          Value: "Course Project public subnet 2"
        - Key: Project
          Value: Course Project
  # Subnets >

  # < Route tables
  publicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: "Course Project VPC Public Route Table"
        - Key: Project
          Value: Course Project

  publicRoute:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref publicRouteTable
      DestinationCidrBlock: "0.0.0.0/0"
      GatewayId: !Ref internetGateway

  publicSubnet0RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref publicSubnet0
      RouteTableId: !Ref publicRouteTable

  publicSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref publicSubnet1
      RouteTableId: !Ref publicRouteTable

  publicSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref publicSubnet2
      RouteTableId: !Ref publicRouteTable
  # Route tables >

  # < DB Subnet Group
  DBSubnetGroup:
    Type: AWS::RDS::DBSubnetGroup
    Properties:
      DBSubnetGroupDescription: "Course Project DB Group"
      SubnetIds:
        - !Ref publicSubnet0
        - !Ref publicSubnet1
        - !Ref publicSubnet2
      Tags:
        - Key: Name
          Value: "course-project-db-subnet-group"
        - Key: Project
          Value: Course Project
  # DB Subnet Group >

  # < S3 Endpoint
  S3Endpoint:
    Type: 'AWS::EC2::VPCEndpoint'
    Properties:
      RouteTableIds:
        - !Ref publicRouteTable
      ServiceName: !Sub 'com.amazonaws.${AWS::Region}.s3'
      VpcId: !Ref VPC
  # S3 Endpoint >
Outputs:
  VPC:
    Description: VPC ID
    Value: !Ref VPC
    Export:
      Name: course-project-vpc
  VPCCidrBlock:
    Description: VPC CIDR Block
    Value: !GetAtt VPC.CidrBlock
    Export:
      Name: course-project-vpc-cidr-block
  publicSubnet0:
    Description: Public subnet 0
    Value: !Ref publicSubnet0
    Export:
      Name: course-project-public-subnet-0
  publicSubnet1:
    Description: Public subnet 1
    Value: !Ref publicSubnet1
    Export:
      Name: course-project-public-subnet-1
  publicSubnet2:
    Description: Public subnet 2
    Value: !Ref publicSubnet2
    Export:
      Name: course-project-public-subnet-2
  dbSubnetGroupId:
    Description: Subnet for DB instances
    Value: !Ref DBSubnetGroup
    Export:
      Name: course-project-db-subnet-group-id